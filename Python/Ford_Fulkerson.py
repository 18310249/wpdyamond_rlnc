#!/usr/bin/env python
#
# http://en.wikipedia.org/wiki/Ford-Fulkerson_algorithm
# Ford-Fulkerson algorithm computes max flow in a flow network.
#

class Edge(object):
  def __init__(self, u, v, w):
    self.source = u
    self.target = v
    self.capacity = w


class FlowNetwork(object):
  def  __init__(self):
    self.adj = {}
    self.flow = {}
    self.disjoint_paths = []

  def AddVertex(self, vertex):
    self.adj[vertex] = []

  def GetEdges(self, v):
    return self.adj[v]

  def AddEdge(self, u, v, w = 0):
    if u == v:
      raise ValueError("u == v")
    edge = Edge(u, v, w)
    redge = Edge(v, u, 0)
    edge.redge = redge
    redge.redge = edge
    self.adj[u].append(edge)
    self.adj[v].append(redge)
    # Intialize all flows to zero
    self.flow[edge] = 0
    self.flow[redge] = 0

  def FindPath(self, source, target, path):
    if source == target:
      return path
    for edge in self.GetEdges(source):
      residual = edge.capacity - self.flow[edge]
      if residual > 0 and not (edge, residual) in path:
        result = self.FindPath(edge.target, target, path + [(edge, residual)])
        if result != None:
          return result

  def MaxFlow(self, source, target):
    path = self.FindPath(source, target, [])
    for i in range(0, len(path)):
       e = [path[i][0].source, path[i][0].target]
       self.disjoint_paths.append(e)
    while path != None:
      flow = min(res for edge, res in path)
      for edge, res in path:
        self.flow[edge] += flow
        self.flow[edge.redge] -= flow
      path = self.FindPath(source, target, [])
      if path is not None:
          for i in range(0, len(path)):
            e = [path[i][0].source, path[i][0].target]
            self.disjoint_paths.append(e)

    return sum(self.flow[edge] for edge in self.GetEdges(source))


def ConfigGraph(V, E):
    # Create network object
    g = FlowNetwork()
    
    # Add nodes
    for n in V:
        g.AddVertex(n)
    
    # Add egdes
    for i in range(0, len(E)):
        for j in range(0, len(E[i])):
            g.AddEdge(E[i][j][0], E[i][j][1], 1)
    
    return g