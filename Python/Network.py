# -*- coding: utf-8 -*-
"""
Created on Wed Aug 16 10:56:53 2017

@author: wayne
"""
import Ford_Fulkerson as flow
from galois import GF

def Setup():
    ################## NETWORK CONFIGURATION ##################
    # Vertices in network
    # T sinks and x other nodes in network
    T = ['t1', 't2', 't3', 't4', 't5', 't6']
    x = ['s', 'u', 'v', 'w', 'x']
    V = x + T      # Resulting vertices V
    
    E = [[['s','u'],['s','v'],['s','w'],['s','x']],
         [['u','t1'],['u','t3'],['u','t6']],
         [['v','t1'],['v','t2'],['v','t5']],
         [['w','t2'],['w','t3'],['w','t4']],
         [['x','t4'],['x','t5'],['x','t6']]]
    
    return V, E, 's', T

def MinCut():
    Ft = []
    # Calculate h, the maximum flow rate / min-cut
    # ft contains h edge-disjoint paths for each t
    g = flow.ConfigGraph(V, E)
    min_cut = g.MaxFlow('s',T[0])
    Ft.append(g.disjoint_paths)
    for t in T[1:]:
        g = flow.ConfigGraph(V, E)
        f = g.MaxFlow('s',t)
        Ft.append(g.disjoint_paths)
        if(f < min_cut):
            min_cut = f
        
    return min_cut, Ft
    
################## MAIN FUNCTION ##################
def LIF(V, E, s, T):
    # Initialization
    h, ft = MinCut()
    gf3 = GF(3)         # Finite field order 3
    
    


# Run setup of network graph
V,E,s,T = Setup()
LIF(V, E, s, T)