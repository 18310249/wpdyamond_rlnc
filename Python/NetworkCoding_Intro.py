# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# Butterfly network - network coding example
# Same as relay node example
# Source transmit m1 and m2
m1 = 0b1
m2 = 0b0

# Intermediary node performs XOR
m3 = m1 ^ m2

# Destination receives m1 / m2 and m3
# Decoded with xor operation
r1 = m1 ^ m3
r2 = m2 ^ m3

print("Source: m1 = " + '{0:b}'.format(m1) + ", m2 = " + '{0:b}'.format(m2))
print("Intermediary: m1 xor m2 = " + '{0:b}'.format(m3))
print("Destination: r1 = " + '{0:b}'.format(m1) + "," +
     '{0:b}'.format(r1) + " ; r2 = " +
     '{0:b}'.format(m2) + "," + '{0:b}'.format(r2))
